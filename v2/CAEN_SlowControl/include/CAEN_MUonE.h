#define CAEN_MUonE_h
#include <iostream>
#include <iomanip>
#include <string>
#include "./CAENHVWrapper.h"



using namespace std;



class CAEN_MUonE{

	public :
		CAEN_MUonE() {}
		CAEN_MUonE(char *pIPaddress, string pUsername, string pPassword);
		~CAEN_MUonE();
		
		bool InitSystem();
		void DeinitSystem();
		void GetCrateMap();
		void GetError();
		float GetIMon(unsigned short pSlot, unsigned short pChannel);
		float GetVMon(unsigned short pSlot, unsigned short pChannel);	
		void SetHV_I0(unsigned short pSlot, unsigned short pChannel, float pI0);
		void SetHV_V0(unsigned short pSlot, unsigned short pChannel, float pV0);
		void SetLV_I0(unsigned short pSlot, unsigned short pChannel, float pI0);
		void SetLV_V0(unsigned short pSlot, unsigned short pChannel, float pV0);
		void SwitchOnChannel(unsigned short pSlot, unsigned short pChannel);
		void SwitchOffChannel(unsigned short pSlot, unsigned short pChannel);

		float GetI0(unsigned short pSlot, unsigned short pChannel);
		float GetV0(unsigned short pSlot, unsigned short pChannel);
		unsigned GetChannelStatus(unsigned short pSlot, unsigned short pChannel);

	private :
		char * fIPaddress;
		string fUsername;
		string fPassword;
		int fSystemHandle = -100;
		CAENHVRESULT fResult = 0x1;

		//CrateMap stuff
		unsigned short fNSlots;//number of slots
		unsigned short *fNChannels;//number of channels
		char           *fModel;//model of the board
		char           *fDescription;//description of the board
		unsigned short *fSerialNumber;//board serial number
		unsigned char  *fFWReleaseLSB;//LSByte of the firmware release
		unsigned char  *fFWReleaseMSB;//MSByte of the firmware release

		const CAENHV_SYSTEM_TYPE_t fSystem = SY4527;
		const int fLinkType = LINKTYPE_TCPIP;



		const string RESET   = "\033[m";
		const string RED     = "\033[31m";
		const string GREEN   = "\033[32m";
		const string YELLOW  = "\033[33m";
		const string BLUE    = "\033[34m";
		const string MAGENTA = "\033[35m";
		const string CYAN    = "\033[36m";
		const string WHITE   = "\033[37m";
		const string BOLDRED     = "\033[1;31m";
		const string BOLDGREEN   = "\033[1;32m";
		const string BOLDYELLOW  = "\033[1;33m";
		const string BOLDBLUE    = "\033[1;34m";
		const string BOLDMAGENTA = "\033[1;35m";
		const string BOLDCYAN    = "\033[1;36m";
		const string BOLDWHITE   = "\033[1;37m";
};
