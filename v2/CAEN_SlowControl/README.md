# INSTALL THE SCRIPTS ON LXPLUS

```
mkdir bin
source compile.sh
```
This will compile the scripts to read HV and LV currents and voltages, to power on HV and LV, to power off HV and LV.


# HOW TO RUN THE SCRIPTS

#### READ HV/IV
This script reads HV/IV from all the modules every 30s, and writes the measurements in this directory: `/eos/experiment/mu-e/daq/monitoring/CAEN_IV_monitoring/`. Output file name can be chosen when lauching the script.
```
./bin/CAENreadIV <ip_address> <username> <password> <output_filename>
```
You can find psu ip address, username and password in the file `compile.sh`

#### POWER ON MODULES
This script powers on HV and LV on all the modules. If a system between HV and LV is already on, the script will power on the other one.
```
./bin/CAENpowerONmodules <ip_address> <username> <password>
```
You can find psu ip address, username and password in the file `compile.sh`

#### POWER OFF MODULES
This script powers off HV and LV on all the modules. If a system between HV and LV is already off, the script will power off the other one.
```
./bin/CAENpowerOFFmodules <ip_address> <username> <password>
```
You can find psu ip address, username and password in the file `compile.sh`

#### SET HV V0
This is an interactive script to set the values of HV V0 for the modules. Once the bias is set, the corresponding channel will be powered on.
```
./bin/CAENsetHV <ip_address> <username> <password>
```
You can find psu ip address, username and password in the file `compile.sh`. Then, follow the instructions on screen to select if you want to set the bias value for all the modules at one time or one module at a time. After that, the script will ask you to insert the desired bias value. Finally, it will set the values and powers on the selected modules, then prints on screen IMon and VMon for 15s to follow the ramp.
