#include "../include/CAEN_MUonE.h"



CAEN_MUonE::CAEN_MUonE(char *pIPaddress, string pUsername, string pPassword) {
	fIPaddress = pIPaddress;
	fUsername  = pUsername;
	fPassword  = pPassword;
}



bool CAEN_MUonE::InitSystem() {

	// cout<<YELLOW<<"Initialize system"<<RESET<<endl;

	int cAttempt = 0;
	int cMaxAttemptsToConnect = 5;

	while(fResult != CAENHV_OK && cAttempt < cMaxAttemptsToConnect) {

		fResult = CAENHV_InitSystem(fSystem, fLinkType, fIPaddress, fUsername.c_str(), fPassword.c_str(), &fSystemHandle);
		
		if(fResult != CAENHV_OK) {
			cout<<"systemHandle = "<<fSystemHandle<<endl;
			cout<<CAENHV_GetError(fSystemHandle)<<endl;
			cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). System cannot be initialized. Retry in 5 seconds";
			for(int i = 0; i < 5; i++) {
				cout<<".";
				sleep(1);
			}
			cout<<RESET<<endl;
			cAttempt++;
		}
	}

	if(fResult != CAENHV_OK) {
		cout<<BOLDRED<<"ERROR "<<fResult<<". Cannot connect to SY4527 after 5 attempts, check the system"<<RESET<<endl;
		return 0;
	}
	// else cout<<BOLDGREEN<<"Ok, we are connected with "<<fIPaddress<<RESET<<endl;

return 1;
}


void CAEN_MUonE::DeinitSystem() {

	fResult = CAENHV_DeinitSystem(fSystemHandle);
	if(fResult != CAENHV_OK) cout<<BOLDRED<<"ERROR "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Unable to deinitialize the system."<<RESET<<endl;
	// else cout<<BOLDGREEN<<"System deinitilized."<<RESET<<endl;

return ;
}



void CAEN_MUonE::GetError() {
	cout<<BOLDYELLOW<<"Error: "<<CAENHV_GetError(fSystemHandle)<<RESET<<endl;
return ;
}



void CAEN_MUonE::GetCrateMap() {

	cout<<YELLOW<<"GetCrateMap..."<<RESET<<endl;

	fResult = CAENHV_GetCrateMap(fSystemHandle, //In
        			    &fNSlots, //Out
			            &fNChannels, //Out
        			    &fModel, //Out
        			    &fDescription, //Out
        			    &fSerialNumber, //Out
        			    &fFWReleaseLSB, //Out
        			    &fFWReleaseMSB);//Out

	if(fResult != CAENHV_OK) {
		cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot create the cratemap."<<RESET<<endl;
		return ;
	}
	else {
		cout<<BOLDGREEN<<"SY4527 CrateMap created!"<<RESET<<endl;
		cout<<BOLDGREEN<<"We have "<<fNSlots<<" slots"<<RESET<<endl;	
		cout<<BOLDMAGENTA<<"Channels list:"<<RESET<<endl;
		for(int i = 0; i < fNSlots; i++) {
			cout<<BOLDMAGENTA<<"****** SLOT "<<i<<" ******"<<RESET<<endl;
			cout<<MAGENTA<<"Board model: "<<fModel[i]<<RESET<<endl;
			cout<<MAGENTA<<"Board description: "<<fDescription[i]<<RESET<<endl;
			cout<<MAGENTA<<"Board #channels: "<<fNChannels[i]<<RESET<<endl;
			cout<<MAGENTA<<"Board serial number: "<<fSerialNumber[i]<<RESET<<endl;
			cout<<MAGENTA<<"Board Firmware LSB: "<<fFWReleaseLSB[i]<<RESET<<endl;
			cout<<MAGENTA<<"Board Firmware MSB: "<<fFWReleaseMSB[i]<<RESET<<endl;
			cout<<BOLDMAGENTA<<"*********************"<<RESET<<endl;
		}
	}

return ;
}



float CAEN_MUonE::GetIMon(unsigned short pSlot, unsigned short pChannel) {

	float cIMon = -999;
	fResult = CAENHV_GetChParam(fSystemHandle, pSlot, "IMon", 1, &pChannel, &cIMon);
	if(fResult != CAENHV_OK) {
		cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot read IMon from slot "<<pSlot<<", channel "<<pChannel<<RESET<<endl;
		return -999;
	}

	return cIMon;
}



float CAEN_MUonE::GetVMon(unsigned short pSlot, unsigned short pChannel) {

	float cVMon = -999;
	fResult = CAENHV_GetChParam(fSystemHandle, pSlot, "VMon", 1, &pChannel, &cVMon);
	if(fResult != CAENHV_OK) {
		cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot read VMon from slot "<<pSlot<<", channel "<<pChannel<<RESET<<endl;
		return -999;
	}

	return cVMon;
}



float CAEN_MUonE::GetI0(unsigned short pSlot, unsigned short pChannel) {

	float cI0 = -999;
	fResult = CAENHV_GetChParam(fSystemHandle, pSlot, "I0Set", 1, &pChannel, &cI0);
	if(fResult != CAENHV_OK) {
		cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot read I0 from slot "<<pSlot<<", channel "<<pChannel<<RESET<<endl;
		return -999;
	}

	return cI0;
}



float CAEN_MUonE::GetV0(unsigned short pSlot, unsigned short pChannel) {

	float cV0 = -999;
	fResult = CAENHV_GetChParam(fSystemHandle, pSlot, "V0Set", 1, &pChannel, &cV0);
	if(fResult != CAENHV_OK) {
		cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot read V0 from slot "<<pSlot<<", channel "<<pChannel<<RESET<<endl;
		return -999;
	}

	return cV0;
}



unsigned CAEN_MUonE::GetChannelStatus(unsigned short pSlot, unsigned short pChannel) {

	unsigned cStatus = -9999;
	fResult = CAENHV_GetChParam(fSystemHandle, pSlot, "Status", 1, &pChannel, &cStatus);
	if(fResult != CAENHV_OK) {
		cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot read Status from slot "<<pSlot<<", channel "<<pChannel<<RESET<<endl;
		return -9999;
	}

	return cStatus;
}



void CAEN_MUonE::SetHV_I0(unsigned short pSlot, unsigned short pChannel, float pI0) {
	if(pSlot != 2) {
		cout<<YELLOW<<"Check the slot... our HV psu is at slot = 2"<<RESET<<endl;
		return ;
	}
	if(pI0 > 50) {
		cout<<BOLDRED<<"You are trying to set I0 > 50 uA for a HV channel... maybe it's too much? Try with a lower current"<<RESET<<endl;
		return ;
	}

	fResult = CAENHV_SetChParam(fSystemHandle, pSlot, "I0Set", 1, &pChannel, &pI0);

        if(fResult != CAENHV_OK) {
                cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot set HV I0 at slot "<<pSlot<<", channel "<<pChannel<<RESET<<endl;
                return ;
        }
	return ;
}



void CAEN_MUonE::SetHV_V0(unsigned short pSlot, unsigned short pChannel, float pV0) {
	if(pSlot != 2) {
		cout<<YELLOW<<"Check the slot... our HV psu is at slot = 2"<<RESET<<endl;
		return ;
	}
	// if(pV0 > 1000) {
	// 	cout<<BOLDRED<<"You are trying to set V0 > 1000 V for a HV channel... are you sure??"<<RESET<<endl;
	// 	return ;
	// }

	fResult = CAENHV_SetChParam(fSystemHandle, pSlot, "V0Set", 1, &pChannel, &pV0);

        if(fResult != CAENHV_OK) {
                cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot set HV V0 at slot "<<pSlot<<", channel "<<pChannel<<RESET<<endl;
                return ;
        }
	return ;
}



void CAEN_MUonE::SetLV_I0(unsigned short pSlot, unsigned short pChannel, float pI0) {
	if(pSlot == 2) {
		cout<<YELLOW<<"Check the slot... slot number 2 is HV. Our LV slots are 7, 8, 9."<<RESET<<endl;
		return ;
	}
	if(pI0 > 2) {
		cout<<YELLOW<<"You are trying to set I0 > 2 A for a LV channel... maybe it's too much? Try with a lower current"<<RESET<<endl;
		return ;
	}

	fResult = CAENHV_SetChParam(fSystemHandle, pSlot, "I0Set", 1, &pChannel, &pI0);

        if(fResult != CAENHV_OK) {
                cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot set LV I0 at slot "<<pSlot<<", channel "<<pChannel<<RESET<<endl;
                return ;
        }
	return ;
}



void CAEN_MUonE::SetLV_V0(unsigned short pSlot, unsigned short pChannel, float pV0) {
	if(pSlot == 2) {
		cout<<YELLOW<<"Check the slot... slot number 2 is HV. Our LV slots are 7, 8, 9."<<RESET<<endl;
		return ;
	}
	if(pV0 > 11 || pV0 < 8) {
		cout<<YELLOW<<"LV V0 is out of range... Set V0 between 8V and 11V"<<RESET<<endl;
		return ;
	}

	fResult = CAENHV_SetChParam(fSystemHandle, pSlot, "V0Set", 1, &pChannel, &pV0);

        if(fResult != CAENHV_OK) {
                cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot set LV V0 at slot "<<pSlot<<", channel "<<pChannel<<RESET<<endl;
                return ;
        }
	return ;
}



void CAEN_MUonE::SwitchOnChannel(unsigned short pSlot, unsigned short pChannel) {
	unsigned cPower = 1;
	fResult = CAENHV_SetChParam(fSystemHandle, pSlot, "Pw", 1, &pChannel, &cPower);

        if(fResult != CAENHV_OK) {
                cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num 0x"<<std::hex<<std::setw(8)<<std::setfill('0')<<fResult<<"). Cannot switch on channel "
		<<std::dec<<pChannel<<" in slot "<<pSlot<<RESET<<endl;
                return ;
        }
        return ;
}



void CAEN_MUonE::SwitchOffChannel(unsigned short pSlot, unsigned short pChannel) {
	unsigned cPower = 0;
	fResult = CAENHV_SetChParam(fSystemHandle, pSlot, "Pw", 1, &pChannel, &cPower);

        if(fResult != CAENHV_OK) {
                cout<<BOLDRED<<"ERROR: "<<CAENHV_GetError(fSystemHandle)<<" (num "<<fResult<<"). Cannot swicth off channel "<<pChannel<<" in slot "<<pSlot<<RESET<<endl;
                return ;
        }
        return ;
}
