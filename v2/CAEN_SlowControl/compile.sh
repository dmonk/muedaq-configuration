#!/bin/sh

IPADDRESS="128.141.41.235"
USERNAME="admin"
PASSWORD="MUonEPSU2022"


OPTIONS="-std=c++11 -O3 -Wall -g"
OUTDIR="./bin/"


MAIN="CAEN_read"
EXE="CAENreadIV"
g++ ${OPTIONS} -lX11 ./${MAIN}.C ./src/CAEN_MUonE.cc ./lib/libcaenhvwrapper.so -o ${OUTDIR}${EXE}
echo $MAIN " compiled!"


MAIN="CAEN_powerONmodules"
EXE="CAENpowerONmodules"
g++ ${OPTIONS} -lX11 ./${MAIN}.C ./src/CAEN_MUonE.cc ./lib/libcaenhvwrapper.so -o ${OUTDIR}${EXE}
echo $MAIN " compiled!"


MAIN="CAEN_powerOFFmodules"
EXE="CAENpowerOFFmodules"
g++ ${OPTIONS} -lX11 ./${MAIN}.C ./src/CAEN_MUonE.cc ./lib/libcaenhvwrapper.so -o ${OUTDIR}${EXE}
echo $MAIN " compiled!"


MAIN="CAEN_setHV"
EXE="CAENsetHV"
g++ ${OPTIONS} -lX11 ./${MAIN}.C ./src/CAEN_MUonE.cc ./lib/libcaenhvwrapper.so -o ${OUTDIR}${EXE}
echo $MAIN " compiled!"
