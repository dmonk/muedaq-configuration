import click
from commands import programFPGA, reconfigureFEModules, configureFirmware, pullPackage, pullConfig, enable10GEcho
import uhal
from ipbtools import writeAddress, resetDTCAligner
import subprocess
from time import sleep
from contextlib import contextmanager
import os
import numpy as np


DEVICE = "x1"


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)



@click.command()
@click.argument("run_id")
@click.option("--reprogram", help="Reprogram the FPGA.", is_flag=True)
@click.option("--reconfigure", help="Reconfigure the FE modules", is_flag=True)
def run(run_id, reprogram, reconfigure):
    config = pullConfig("mongodb://root:example@muedaq-supervisor", run_id)
    #click.echo(config)
    package = "/home/cmx/.emp/packages/{}/{}/package".format(config['package']['image'], config['package']['tag'])
    if reprogram:
        pullPackage(config['package']['image'], config['package']['tag'])
        click.echo("Reprogramming the device with package {}...".format(package))
        programFPGA(package, DEVICE)
    enable10GEcho(package, DEVICE)
    if reconfigure:
        click.echo("Reconfiguring the FE modules...")
        reconfigureFEModules(
            "mongodb://root:example@muedaq-supervisor", run_id,
            threshold=config['fe-modules']['threshold'],
            reconfigure=config['fe-modules']['reconfigure'],
            disablePerugiaChip7=config['fe-modules']['disablePerugiaChip7']
        )
    click.echo("Configuring the firwmare with database entry {}...".format(run_id))
    configureFirmware(package, DEVICE, "mongodb://root:example@muedaq-supervisor", run_id)
    
    #Custom commands
    click.echo("Waiting 60 seconds to allow for spoolers to be started...")
    sleep(60)

    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(DEVICE)

    for i in np.arange(0, 24, 1):
        dll = i
        click.echo("Setting dll to: {}".format(dll))
        writeAddress(hw, "payload.link_combiner.control.enable", 0)

        with cd("~/dtc/Ph2_ACF"):
          subprocess.call("./bin/serenity_2slpgbt --setModule=2 --setDLL={}".format(dll+1), shell=True)
          subprocess.call("./bin/serenity_2slpgbt --setModule=3 --setDLL={}".format(dll+4), shell=True)
          subprocess.call("./bin/serenity_2slpgbt --setModule=4 --setDLL={}".format(dll+4), shell=True)
          writeAddress(hw, "payload.link_combiner.header_user_bits", dll,  verbose=True)

        for mod in range(4):
          click.echo("Resetting FE link {}...".format(mod))
          resetDTCAligner(hw, mod)

        sleep(1)
        writeAddress(hw, "payload.link_combiner.control.enable", 1)
        raw_input("Enough data collected? Press any key to continue to next step...")


@click.group()
def main():
    pass


if __name__ == "__main__":
    uhal.disableLogging()
    main.add_command(run)
    main()
