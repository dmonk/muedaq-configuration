import click
from commands import programFPGA, reconfigureFEModules, configureFirmware, pullPackage, pullConfig, enable10GEcho
import uhal
from ipbtools import writeAddress
import subprocess
from time import sleep
from contextlib import contextmanager
import os
import numpy as np


DEVICE = "x1"


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)



@click.command()
@click.argument("run_id")
@click.option("--reprogram", help="Reprogram the FPGA.", is_flag=True)
@click.option("--reconfigure", help="Reconfigure the FE modules", is_flag=True)
@click.option("--module", help="Specify module to scan")
def run(run_id, reprogram, reconfigure, module):
    config = pullConfig("mongodb://root:example@muedaq-supervisor", run_id)
    #click.echo(config)
    package = "/home/cmx/.emp/packages/{}/{}/package".format(config['package']['image'], config['package']['tag'])
    if reprogram:
        pullPackage(config['package']['image'], config['package']['tag'])
        click.echo("Reprogramming the device with package {}...".format(package))
        programFPGA(package, DEVICE)
    enable10GEcho(package, DEVICE)
    if reconfigure:
        click.echo("Reconfiguring the FE modules...")
        reconfigureFEModules(
            "mongodb://root:example@muedaq-supervisor", run_id,
            threshold=config['fe-modules']['threshold'],
            reconfigure=config['fe-modules']['reconfigure'],
            disablePerugiaChip7=config['fe-modules']['disablePerugiaChip7']
        )
    click.echo("Configuring the firwmare with database entry {}...".format(run_id))
    configureFirmware(package, DEVICE, "mongodb://root:example@muedaq-supervisor", run_id)
    
    #Custom commands
    click.echo("Waiting 60 seconds to allow for spoolers to be started...")
    sleep(60)

    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(DEVICE)

    with cd("./CAEN_SlowControl"):
        subprocess.call("./bin/CAENsetHVscript 128.141.41.235 admin MUonEPSU2022 {} {}".format(module, 0), shell=True)
        writeAddress(hw, "payload.link_combiner.header_user_bits", 0,  verbose=True)
    click.echo("Waiting another 60 seconds for the bias to drop to 0...")
    sleep(60)
    click.echo("Starting custom commands...")

    writeAddress(hw, "payload.link_combiner.header_user_bits", 0,  verbose=True)
    for i in np.arange(0, 30, 1):
        bias = int(i**2)
        click.echo("Setting bias voltage to: {} V".format(bias))
        with cd("./CAEN_SlowControl"):
            subprocess.call("./bin/CAENsetHVscript 128.141.41.235 admin MUonEPSU2022 {} {}".format(module, bias), shell=True)
            writeAddress(hw, "payload.link_combiner.header_user_bits", bias,  verbose=True)
        sleep(60)


@click.group()
def main():
    pass


if __name__ == "__main__":
    uhal.disableLogging()
    main.add_command(run)
    main()