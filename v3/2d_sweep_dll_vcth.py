import click
from commands import programFPGA, reconfigureFEModules, configureFirmware, pullPackage, pullConfig, enable10GEcho
import uhal
from ipbtools import writeAddress, resetDTCAligner
import subprocess
from time import sleep
from contextlib import contextmanager
import os
import numpy as np


DEVICE = "x1"


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def getHVVoltage(module):
    with cd("./CAEN_SlowControl"):
        value = float(subprocess.check_output("./bin/CAENgetHVscript 128.141.41.235 admin MUonEPSU2022 {}".format(module), shell=True).strip())
        sleep(1)
        return value


@click.command()
@click.argument("run_id")
@click.option("--reprogram", help="Reprogram the FPGA.", is_flag=True)
@click.option("--reconfigure", help="Reconfigure the FE modules", is_flag=True)
@click.option("--module", help="Specify module to scan")
def run(run_id, reprogram, reconfigure, module):
    config = pullConfig("mongodb://root:example@muedaq-supervisor", run_id)
    #click.echo(config)
    package = "/home/cmx/.emp/packages/{}/{}/package".format(config['package']['image'], config['package']['tag'])
    if reprogram:
        pullPackage(config['package']['image'], config['package']['tag'])
        click.echo("Reprogramming the device with package {}...".format(package))
        programFPGA(package, DEVICE)
    enable10GEcho(package, DEVICE)
    if reconfigure:
        click.echo("Reconfiguring the FE modules...")
        reconfigureFEModules(
            "mongodb://root:example@muedaq-supervisor", run_id,
            threshold=config['fe-modules']['threshold'],
            reconfigure=config['fe-modules']['reconfigure'],
            disablePerugiaChip7=config['fe-modules']['disablePerugiaChip7']
        )
    click.echo("Configuring the firwmare with database entry {}...".format(run_id))
    configureFirmware(package, DEVICE, "mongodb://root:example@muedaq-supervisor", run_id)
    
    #Custom commands
    click.confirm('Have spoolers been started?', abort=True)

    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(DEVICE)

    for i in np.arange(0, 24, 1):
        dll = i + 1
        click.echo("Setting dll to: {}".format(dll))
        writeAddress(hw, "payload.mprocessor.link_combiner.control.enable", 0)

        with cd("~/dtc/Ph2_ACF"):
            subprocess.call("./bin/serenity_2slpgbt --setModule={} --setDLL={}".format(module, dll), shell=True)

        for j in range(4):
            resetDTCAligner(hw, j)
        sleep(1)
        writeAddress(hw, "payload.mprocessor.link_combiner.control.enable", 1)
        for j in np.arange(0, 41, 4):
            threshold = 400 + int(j*5)
            click.echo("Setting threshold to: {}".format(threshold))
            with cd("~/dtc/Ph2_ACF"):
                subprocess.call("./bin/serenity_2slpgbt --setModule={} --setThreshold={}".format(module, threshold), shell=True)
            writeAddress(hw, "payload.mprocessor.link_combiner.header_user_bits", threshold + (dll << 16),  verbose=True)
            sleep(60)


@click.group()
def main():
    pass


if __name__ == "__main__":
    uhal.disableLogging()
    main.add_command(run)
    main()
