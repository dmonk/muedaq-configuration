import subprocess
import uhal
from ipbtools import resetDTCAligner, resetMProcessor, writeAddress, readAddress
import os
from pymongo import MongoClient
from bson.objectid import ObjectId
from func import dotter, convertValue, buildXML
from time import sleep
import click
from func import cd
from copy import deepcopy
import sys
import xml.etree.cElementTree as ET
from collections import OrderedDict


def pullConfig(connection_string, _id):
    client = MongoClient(connection_string)
    db = client['muone']
    collection = db['serenity-configs']
    item = collection.find_one({"_id": ObjectId(_id)})
    client.close()
    return item


def pullRegisterConfig(connection_string, _id):
    client = MongoClient(connection_string)
    db = client['muone']
    collection = db['serenity-configs']
    item = collection.find_one({"_id": ObjectId(_id)})
    client.close()
    return dotter(item['registers'], {})


def pullPackage(image, tag):
    subprocess.call("emp pull {}:{}".format(image, tag), shell=True)


def programFPGA(package, device, reset_clocks=True):
    subprocess.call("~/dtc/dtc_init.sh {} -{} {}".format(
        "-r" if reset_clocks else "", device[1:], package
    ), shell=True)


def reconfigureFEModules(connection_string, run_id, threshold=550, reconfigure=True, disablePerugiaChip7=True):
    client = MongoClient(connection_string, document_class=OrderedDict)
    db = client['muone']
    collection = db['serenity-configs']
    item = collection.find_one({"_id": ObjectId(run_id)})
    collection = db["ph2_acf-configs"]
    click.echo("Getting Ph2_ACF settings file {}...".format(item["ph2_acf-settings"]))
    settings_file = collection.find_one({"_id": item['ph2_acf-settings']})
    HwDescription = ET.Element("HwDescription")
    buildXML(settings_file["config"]["HwDescription"]["BeBoard"], HwDescription, "BeBoard")
    tree = ET.ElementTree(HwDescription)
    #ET.indent(tree, space="\t", level=0)
    tree.write("/home/cmx/dtc/Ph2_ACF/settings/SerenityHWDescription2SLpGbt.xml", encoding='utf-8', xml_declaration=True)
    client.close()
    with cd("~/dtc/Ph2_ACF"):
        child = subprocess.call(
            "./bin/serenity_2slpgbt {} --setThreshold {} {}".format(
                "--reconfigure" if reconfigure else "",
                threshold,
                "--disablePerugiaChip7" if disablePerugiaChip7 else ""
            ), 
            shell=True
        )

def enable10GEcho(package, device):
    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(device)
    writeAddress(hw, "eth10g.channel_0.ctrl.local_ip", 0xC0A80452)
    writeAddress(hw, "eth10g.channel_0.ctrl.remote_ip", 0xC0A80402)
    writeAddress(hw, "eth10g.channel_0.ctrl.ports.local", 0x04D2)
    writeAddress(hw, "eth10g.channel_0.ctrl.ports.remote", 0x04D2)
    writeAddress(hw, "eth10g.channel_0.ctrl.reg.echo", 1)
    writeAddress(hw, "eth10g.channel_1.ctrl.local_ip", 0xC0A80352)
    writeAddress(hw, "eth10g.channel_1.ctrl.remote_ip", 0xC0A80302)
    writeAddress(hw, "eth10g.channel_1.ctrl.ports.local", 0x04D2)
    writeAddress(hw, "eth10g.channel_1.ctrl.ports.remote", 0x04D2)
    writeAddress(hw, "eth10g.channel_1.ctrl.reg.echo", 1)


def configureFirmware(package, device, connection_string, run_id):
    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(device)
    registers = pullConfig(connection_string, run_id)['registers']
    # Reset FE links
    for i in range(4):
        click.echo("Resetting FE link {}...".format(i))
        resetDTCAligner(hw, i)
    # Disable and reset mprocessor
    for i in range(len(registers['payload']['mprocessor'])):
        resetMProcessor(hw, i)
    # Configure 10g ethernet
    if "heartbeat" in registers['eth10g']['channel_0']['ctrl']['reg']:
        heartbeat_enable = convertValue(registers['eth10g']['channel_0']['ctrl']['reg'].pop("heartbeat"))
    else:
        heartbeat_enable = None
    registers_10g = dotter(registers['eth10g'], {}, prefix="eth10g.")
    for path, value in registers_10g.items():
        writeAddress(hw, str(path), convertValue(value), verbose=True)
    sleep(1)
    if heartbeat_enable is not None:
        writeAddress(hw, "eth10g.channel_0.ctrl.reg.heartbeat", heartbeat_enable, verbose=True)
        writeAddress(hw, "eth10g.channel_1.ctrl.reg.heartbeat", heartbeat_enable, verbose=True)
    # Configure mprocessor
    for i, d in enumerate(registers['payload']['mprocessor']):
        writeAddress(hw, "payload.mproc_ctrl.chan_sel", i, verbose=True)
        registers_mprocessor = dotter(d, {}, prefix="payload.mprocessor.")
        for path, value in registers_mprocessor.items():
            writeAddress(hw, str(path), convertValue(value), verbose=True)

    # Configure Histogram csr
    registers_csr = dotter(registers['payload']['csr'], {}, prefix="payload.csr.")
    for path, value in registers_csr.items():
        writeAddress(hw, str(path), convertValue(value), verbose=True)


def waitForHistogram(hwdevice, seconds, cycles=3):
    histogram_cycles = [0, 0]
    bin_value, bin_value_previous = [0, 0], [0, 0]
    for i in range(int(seconds*2)):
        bin_value_previous = deepcopy(bin_value)
        bin_value[0] = readAddress(hwdevice, "payload.csr.histogram0").value()
        bin_value[1] = readAddress(hwdevice, "payload.csr.histogram1").value()
        for i, value in enumerate(bin_value):
            if value < bin_value_previous[i]:
                histogram_cycles[i] += 1
        if min(histogram_cycles) >= cycles:
            break
        sys.stdout.write("\rHistogram max bins | 0: {:08x} | 1: {:08x}".format(*bin_value))
        sys.stdout.flush()
        sleep(0.5)
    print("\n")
