import click
from commands import programFPGA, reconfigureFEModules, configureFirmware, pullPackage, pullConfig, enable10GEcho
import uhal
import subprocess
import itertools
import numpy as np
from time import sleep


DEVICE = "x1"


def checkOutputForStubs(output):
    lines = output.split("\n")
    a = [i for (i, val) in enumerate(lines) if "1v" in val]
    b = [e-i for i, e in enumerate(a)]
    return np.any(np.array([sum(1 for _ in g) for _, g in itertools.groupby(b)]) > 3)


@click.command()
@click.argument("run_id")
@click.option("--reprogram", help="Reprogram the FPGA.", is_flag=True)
@click.option("--reconfigure", help="Reconfigure the FE modules", is_flag=True)
def run(run_id, reprogram, reconfigure):
    config = pullConfig("mongodb://root:example@muedaq-supervisor", run_id)
    #click.echo(config)
    package = "/home/cmx/.emp/packages/{}/{}/package".format(config['package']['image'], config['package']['tag'])
    if reprogram:
        pullPackage(config['package']['image'], config['package']['tag'])
        click.echo("Reprogramming the device with package {}...".format(package))
        programFPGA(package, DEVICE)
    enable10GEcho(package, DEVICE)
    if reconfigure:
        click.echo("Reconfiguring the FE modules...")
        reconfigureFEModules(
            "mongodb://root:example@muedaq-supervisor", run_id,
            threshold=config['fe-modules']['threshold'],
            reconfigure=config['fe-modules']['reconfigure'],
            disablePerugiaChip7=config['fe-modules']['disablePerugiaChip7']
        )
    click.echo("Configuring the firwmare with database entry {}...".format(run_id))
    configureFirmware(package, DEVICE, "mongodb://root:example@muedaq-supervisor", run_id)

    subprocess.call(
        "empbutler -c /home/cmx/.emp/packages/{}/{}/package/connections.xml do x0 buffers tx Capture -c 24-25".format(
            config['package']['image'], config['package']['tag']
        ),
        shell=True
    )
    while True:
        output = subprocess.check_output(
            "empbutler -c /home/cmx/.emp/packages/{}/{}/package/connections.xml do x1 capture --tx 24-25 && cat data/tx_summary.txt".format(
                config['package']['image'], config['package']['tag']
            ),
            shell=True
        )    
        if checkOutputForStubs(output):
            print output
        sleep(1)

    


@click.group()
def main():
    pass


if __name__ == "__main__":
    uhal.disableLogging()
    main.add_command(run)
    main()
