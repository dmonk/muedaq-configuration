from contextlib import contextmanager
import os
import xml.etree.cElementTree as ET
import collections


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def dotter(mixed, dots, key='', prefix=''):
    if isinstance(mixed, dict):
        for k, v in mixed.items():
            dotter(mixed[k], dots, '%s.%s' % (key, k) if key else k, prefix=prefix)
    else:
        dots[prefix + key] = mixed
    return dots


def convertValue(value):
    if isinstance(value, bool):
        return int(value)
    elif isinstance(value, int):
        return value
    elif isinstance(value, str) or isinstance(value, bytes):
        if value.startswith("0x"):
            return int(value[2:], 16)
        elif value.startswith("0b"):
            return int(value[2:], 2)


def buildXML(config, element, name):
    if type(config) == collections.OrderedDict:
        popped_subelements = collections.OrderedDict()
        indices_to_pop = []
        for i, val in config.items():
            if type(val) != bytes:
                indices_to_pop.append(i)
        for i in indices_to_pop:
            popped_subelements[i] = config.pop(i)
        se = ET.SubElement(element, name, **config)
        for i, val in popped_subelements.items():
            buildXML(val, se, i)
    if type(config) == list:
        for item in config:
            popped_subelements = collections.OrderedDict()
            indices_to_pop = []
            for i, val in item.items():
                if type(val) != bytes:
                    indices_to_pop.append(i)
            for i in indices_to_pop:
                popped_subelements[i] = item.pop(i)
            se = ET.SubElement(element, name[:-1], **item)
            for i, val in popped_subelements.items():
                buildXML(val, se, i)
