import click
from time import sleep


STATUS_MAP = {0: "ERROR", 1: "UNLOCKED", 2: "LOCKING", 4: "LOCKED"}


def readAddress(device, address):
    data = device.getNode(address).read()
    device.dispatch()
    return data


def writeAddress(device, address, value, verbose=False):
    device.getNode(address).write(value)
    device.dispatch()
    read_value = readAddress(device, address)
    assert read_value == value
    if verbose:
        click.echo("{}: 0x{:x}".format(address, read_value))


def readMemory(manager, address, width):
    data = manager.getNode(address).readBlock(int(2**width))
    manager.dispatch()
    return data


def resetMProcessor(hw, id=0):
    writeAddress(hw, "payload.mproc_ctrl.chan_sel", id)
    writeAddress(hw, "payload.mprocessor.link_aggregator.control.input_link_mask", 0b0)
    writeAddress(hw, "payload.mprocessor.link_combiner.control.input_link_mask", 0b0)
    writeAddress(hw, "payload.mprocessor.link_combiner.control.enable", 0)
    writeAddress(hw, "payload.mprocessor.link_aggregator.control.reset", 1)
    writeAddress(hw, "payload.mprocessor.link_aggregator.control.reset", 0)
    writeAddress(hw, "payload.mprocessor.link_combiner.control.reset", 1)
    writeAddress(hw, "payload.mprocessor.link_combiner.control.reset", 0)


def statusColor(status):
    if status == 4:
      status_color = "green"
    elif status == 2 or status == 1:
      status_color = "yellow"
    else:
      status_color = "red"
    return status_color


def resetDTCAligner(hwdevice, channel=0):
    sleep_time = 0.1
    writeAddress(hwdevice, "payload.fe_ctrl.chan_sel", channel)
    sleep(sleep_time)
    writeAddress(hwdevice, "payload.fe_chan.aligner_reset0", 1)
    sleep(sleep_time)
    writeAddress(hwdevice, "payload.fe_chan.aligner_reset1", 1)
    sleep(sleep_time)
    status0 = readAddress(hwdevice, "payload.fe_chan.aligner_status0").value()
    status0_color = statusColor(status0)
    sleep(sleep_time)
    status1 = readAddress(hwdevice, "payload.fe_chan.aligner_status1").value()
    sleep(sleep_time)
    status1_color = statusColor(status1)
    click.echo("Aligner status: 0: {} | 1: {}".format(
        click.style(STATUS_MAP[status0], fg=status0_color), click.style(STATUS_MAP[status1], fg=status1_color)
    ))
    writeAddress(hwdevice, "payload.fe_chan.aligner_reset0", 0)
    sleep(sleep_time)
    writeAddress(hwdevice, "payload.fe_chan.aligner_reset1", 0)
    sleep(sleep_time)
    status0 = readAddress(hwdevice, "payload.fe_chan.aligner_status0").value()
    status0_color = statusColor(status0)
    sleep(sleep_time)
    status1 = readAddress(hwdevice, "payload.fe_chan.aligner_status1").value()
    status1_color = statusColor(status1)
    sleep(sleep_time)
    click.echo("Aligner status: 0: {} | 1: {}".format(
      click.style(STATUS_MAP[status0], fg=status0_color), click.style(STATUS_MAP[status1], fg=status1_color)
    ))
