from contextlib import contextmanager
import os


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def dotter(mixed, dots, key=''):
    if isinstance(mixed, dict):
        for k, v in mixed.items():
            dotter(mixed[k], dots, '%s.%s' % (key, k) if key else k)
    else:
        dots[key] = mixed
    return dots


def convertValue(value):
    if isinstance(value, bool):
        return int(value)
    elif isinstance(value, int):
        return value
    elif isinstance(value, str) or isinstance(value, unicode):
        if value.startswith("0x"):
            return int(value[2:], 16)
        elif value.startswith("0b"):
            return int(value[2:], 2)
