import subprocess
import uhal
from ipbtools import resetDTCAligner, resetMProcessor, writeAddress, readAddress
import os
from pymongo import MongoClient
from bson.objectid import ObjectId
from func import dotter, convertValue
from time import sleep
import click
from func import cd
from copy import deepcopy
import sys


def pullConfig(connection_string, _id):
    client = MongoClient(connection_string)
    db = client['muone']
    collection = db['serenity-configs']
    item = collection.find_one({"_id": ObjectId(_id)})
    client.close()
    return item


def pullRegisterConfig(connection_string, _id):
    client = MongoClient(connection_string)
    db = client['muone']
    collection = db['serenity-configs']
    item = collection.find_one({"_id": ObjectId(_id)})
    client.close()
    return dotter(item['registers'], {})


def pullPackage(image, tag):
    subprocess.call("emp pull {}:{}".format(image, tag), shell=True)


def programFPGA(package, device, reset_clocks=True):
    subprocess.call("~/dtc/dtc_init.sh {} -{} {}".format(
        "-r" if reset_clocks else "", device[1:], package
    ), shell=True)


def reconfigureFEModules(threshold=550, reconfigure=True, disablePerugiaChip7=True):
    with cd("~/dtc/Ph2_ACF"):
        child = subprocess.call(
            "./bin/serenity_2slpgbt {} --setThreshold {} {}".format(
                "--reconfigure" if reconfigure else "",
                threshold,
                "--disablePerugiaChip7" if disablePerugiaChip7 else ""
            ), 
            shell=True
        )

def enable10GEcho(package, device):
    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(device)
    writeAddress(hw, "eth10g.ctrl.local_ip", 0xC0A80352)
    writeAddress(hw, "eth10g.ctrl.remote_ip", 0xC0A80302)
    writeAddress(hw, "eth10g.ctrl.ports.local", 0x04D2)
    writeAddress(hw, "eth10g.ctrl.ports.remote", 0x04D2)
    writeAddress(hw, "eth10g.ctrl.reg.echo", 1)


def configureFirmware(package, device, connection_string, run_id):
    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(device)
    registers = pullRegisterConfig(connection_string, run_id)
    # Reset FE links
    for i in range(4):
        click.echo("Resetting FE link {}...".format(i))
        resetDTCAligner(hw, i)
    # Disable and reset mprocessor
    resetMProcessor(hw)
    # Configure 10g ethernet
    if "eth10g.ctrl.reg.heartbeat" in registers:
        heartbeat_enable = convertValue(registers.pop("eth10g.ctrl.reg.heartbeat"))
    else:
        heartbeat_enable = None
    for path, value in registers.items():
        if path.startswith("eth10g"):
            writeAddress(hw, str(path), convertValue(value), verbose=True)
    sleep(1)
    if heartbeat_enable is not None:
        writeAddress(hw, "eth10g.ctrl.reg.heartbeat", heartbeat_enable, verbose=True)
    # Configure mprocessor
    for path, value in registers.items():
        if path.startswith("payload.mprocessor"):
            writeAddress(hw, str(path), convertValue(value), verbose=True)
    # Configure Histogram csr
    for path, value in registers.items():
        if path.startswith("payload.csr"):
            writeAddress(hw, str(path), convertValue(value), verbose=True)


def waitForHistogram(hwdevice, seconds, cycles=3):
    histogram_cycles = [0, 0]
    bin_value, bin_value_previous = [0, 0], [0, 0]
    for i in range(int(seconds*2)):
        bin_value_previous = deepcopy(bin_value)
        bin_value[0] = readAddress(hwdevice, "payload.csr.histogram0").value()
        bin_value[1] = readAddress(hwdevice, "payload.csr.histogram1").value()
        for i, value in enumerate(bin_value):
            if value < bin_value_previous[i]:
                histogram_cycles[i] += 1
        if min(histogram_cycles) >= cycles:
            break
        sys.stdout.write("\rHistogram max bins | 0: {:08x} | 1: {:08x}".format(*bin_value))
        sys.stdout.flush()
        sleep(0.5)
    print("\n")
