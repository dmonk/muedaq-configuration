import uhal
from func import dotter, convertValue
from time import sleep
from ipbtools import resetDTCAligner, writeAddress
from commands import resetMProcessor

uhal.disableLogging()

config = {'registers': {'eth10g': {'ctrl': {'local_ip': '0xC0A80352', 'reg': {'aggregate_limit': '0x101', 'run': 53, 'echo': 0, 'rst': 0, 'aggregate': 1, 'heartbeat': 1}, 'heartbeat_threshold': '0x13000000', 'watchdog_threshold': '0x80', 'ports': {'remote': '0x04D2', 'local': '0x04D2'}, 'remote_ip': '0xC0A80302'}}, 'payload': {'mprocessor': {'input_link_mask': {'head_start_select': 2, 'link_aggregator': '0b1111', 'link_combiner': '0b11111111'}, 'header_user_bits': '0xd451d007', 'link_combiner_control': {'enable': 1}}, 'csr': {'histogram_sel': 0, 'windowH': 1, 'windowL': 0}}}, 'fe-modules': {'threshold': 550, 'reconfigure': True, 'disablePerugiaChip7': True}, 'package': {'image': 'mprocessor', 'tag': 'reduced-histogram-size-7453c79b'}, '_id': {'$oid': '619186fffab882198302465a'}, 'created': {'$date': '2021-11-14T22:00:31.415Z'}} 

package='/home/cmx/.emp/packages/mprocessor/dmonk-fix-dependencies-309e492f/package'
manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
hw = manager.getDevice("x1")
registers = dotter(config['registers'], {})
sleep(1)
resetDTCAligner(hw, 0)
sleep(1)
resetDTCAligner(hw, 2)
sleep(1)
resetMProcessor(hw)
sleep(1)
if "eth10g.ctrl.reg.heartbeat" in registers:
  heartbeat_enable = convertValue(registers.pop("eth10g.ctrl.reg.heartbeat"))
else:
  heartbeat_enable = None
for path, value in registers.items():
  if path.startswith("eth10g"):
    writeAddress(hw, str(path), convertValue(value), verbose=True)
sleep(1)
if heartbeat_enable is not None:
  writeAddress(hw, "eth10g.ctrl.reg.heartbeat", heartbeat_enable, verbose=True)
# Configure mprocessor
for path, value in registers.items():
  if path.startswith("payload.mprocessor"):
    writeAddress(hw, str(path), convertValue(value), verbose=True)
# Configure Histogram csr
for path, value in registers.items():
  if path.startswith("payload.csr"):
    writeAddress(hw, str(path), convertValue(value), verbose=True)
